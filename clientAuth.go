package certs

import (
	"crypto/x509/pkix"
	"fmt"
	"path"
	"strings"
	"time"

	actions "gitlab.com/digitalxero/simple-actions"
)

// NewCreateClientAuthCertAction docs
func NewCreateClientAuthCertAction(certsDir, certPrefix, certName, caFile, caKey string, notAfter time.Duration) actions.Action {
	if certPrefix != "" && !strings.HasSuffix(certPrefix, "-") {
		certPrefix = fmt.Sprintf("%s-", certPrefix)
	}
	return actions.Actions{
		NewCreatePrivateKeyAction(certsDir, fmt.Sprintf("%s%s", certPrefix, certName)),
		&createCertAction{
			certsDir:     certsDir,
			keyFile:      path.Join(certsDir, fmt.Sprintf("%s%s.key", certPrefix, certName)),
			certFile:     path.Join(certsDir, fmt.Sprintf("%s%s.crt", certPrefix, certName)),
			caFile:       caFile,
			caKey:        caKey,
			isClientAuth: true,
			dnsNames:     []string{certName},
			caSubject: pkix.Name{
				CommonName: certName,
			},
			notBefore: time.Now(),
			notAfter:  time.Now().Add(notAfter),
		},
	}
}
