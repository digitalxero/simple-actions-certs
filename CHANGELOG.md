

<!--- next entry here -->

## 1.0.2
2021-02-25

### Fixes

- update deps (93cee8c54742a93d3a7b3a49acb07ae723845bd1)

## 1.0.1
2021-02-25

### Fixes

- update deps (cd5c156ed527386fb3249523ee5e91bde42df070)

## 1.0.0
2020-05-27

### Fixes

- add missing changelog file (6354e3f67bed3cbdcbb6ac3239f86dfb6ab085dc)
- dep update (7d0300dd96ec0e6aedf83148767993619af0dc89)

## 1.0.0
2020-05-26

### Fixes

- add missing changelog file (6354e3f67bed3cbdcbb6ac3239f86dfb6ab085dc)

