module gitlab.com/digitalxero/simple-actions-certs

go 1.13

require (
	github.com/google/go-cmp v0.4.0 // indirect
	gitlab.com/digitalxero/simple-actions v1.5.0
)
