package certs

import (
	"crypto/x509/pkix"
	"path"
	"time"

	actions "gitlab.com/digitalxero/simple-actions"
)

// NewCreateRootCertAction docs
func NewCreateRootCertAction(certsDir string, subject *pkix.Name) actions.Action {
	if subject == nil {
		subject = &pkix.Name{
			Country:      []string{"US"},
			Organization: []string{"Example Co."},
			CommonName:   "Root CA",
		}
	}
	return actions.Actions{
		NewCreatePrivateKeyAction(certsDir, "ca"),
		&createCertAction{
			certsDir:  certsDir,
			keyFile:   path.Join(certsDir, "ca.key"),
			certFile:  path.Join(certsDir, "ca.crt"),
			isCA:      true,
			caSubject: *subject,
			notBefore: time.Now(),
			notAfter:  time.Now().Add(time.Hour * 24 * 730),
		},
	}
}
