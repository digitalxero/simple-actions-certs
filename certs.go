// Package certs deals with actions around creating certs
package certs

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"path"
	"time"

	actions "gitlab.com/digitalxero/simple-actions"
)

type createCertAction struct {
	certsDir,
	caFile,
	caKey,
	keyFile,
	certFile string
	isCA,
	isClientAuth bool
	dnsNames  []string
	caSubject pkix.Name
	notBefore,
	notAfter time.Time
}

// NewCreateCertAction docs
func NewCreateCertAction(certsDir, certName, caFile, caKey string, notAfter time.Duration, dnsNames ...string) actions.Action {
	return &createCertAction{
		certsDir:  certsDir,
		keyFile:   path.Join(certsDir, fmt.Sprintf("%s.key", certName)),
		certFile:  path.Join(certsDir, fmt.Sprintf("%s.crt", certName)),
		caFile:    caFile,
		caKey:     caKey,
		dnsNames:  dnsNames,
		notBefore: time.Now(),
		notAfter:  time.Now().Add(notAfter),
	}
}

// Execute docs
// nolint:funlen
func (a *createCertAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		rootTemplate = x509.Certificate{
			SerialNumber:          big.NewInt(1),
			Subject:               a.caSubject,
			KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
			BasicConstraintsValid: true,
			IsCA:                  true,
			MaxPathLen:            2,
			IPAddresses:           []net.IP{net.IPv4(127, 0, 0, 1)},
		}

		certTemplate = x509.Certificate{
			KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
			BasicConstraintsValid: true,
			DNSNames:              []string{"localhost"},
			IPAddresses:           []net.IP{net.IPv4(127, 0, 0, 1)},
		}

		clientAuthTemplate = x509.Certificate{
			Subject:               a.caSubject,
			KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth},
			BasicConstraintsValid: true,
			DNSNames:              nil,
		}
	)

	ctx.Status().Start(fmt.Sprintf("Creating cert: %s", a.certFile))
	defer func() {
		ctx.Status().End(err == nil)
		if err != nil {
			_ = os.Remove(a.certFile)
		}
	}()
	if ctx.IsDryRun() {
		return nil
	}

	if !actions.FileExists(a.keyFile) {
		err = fmt.Errorf("private key %s does not exist", a.keyFile)
		return err
	}

	if actions.FileExists(a.certFile) {
		return nil
	}

	var (
		privBlock,
		rootBlock *pem.Block
		priv *rsa.PrivateKey
		pub  interface{}
		privPEM,
		rootPEM,
		cert []byte
		sn       *big.Int
		caCert   *x509.Certificate
		template x509.Certificate
	)
	if privPEM, err = ioutil.ReadFile(a.keyFile); err != nil {
		return err
	}
	if privBlock, _ = pem.Decode(privPEM); privBlock == nil {
		err = fmt.Errorf("failed to parse PEM block containing the key")
		return err
	}
	if priv, err = x509.ParsePKCS1PrivateKey(privBlock.Bytes); err != nil {
		return err
	}
	if sn, err = rand.Prime(rand.Reader, 64); err != nil {
		return err
	}

	pub = publicKey(priv)

	if !a.isCA {
		if rootPEM, err = ioutil.ReadFile(a.caFile); err != nil {
			return err
		}
		if rootBlock, _ = pem.Decode(rootPEM); rootBlock == nil {
			err = fmt.Errorf("failed to parse PEM block containing the key")
			return err
		}
		if caCert, err = x509.ParseCertificate(rootBlock.Bytes); err != nil {
			return err
		}
		if rootPEM, err = ioutil.ReadFile(a.caKey); err != nil {
			return err
		}
		if rootBlock, _ = pem.Decode(rootPEM); rootBlock == nil {
			err = fmt.Errorf("failed to parse PEM block containing the key")
			return err
		}
		if priv, err = x509.ParsePKCS1PrivateKey(rootBlock.Bytes); err != nil {
			return err
		}
	}

	switch {
	case a.isCA:
		template = rootTemplate
		caCert = &rootTemplate
	case a.isClientAuth:
		template = clientAuthTemplate
	default:
		template = certTemplate
	}

	template.SerialNumber = sn
	template.NotBefore = a.notBefore
	template.NotAfter = a.notAfter
	template.DNSNames = append(template.DNSNames, a.dnsNames...)

	if cert, err = x509.CreateCertificate(rand.Reader, &template, caCert, pub, priv); err != nil {
		return err
	}

	out := &bytes.Buffer{}
	if err = pem.Encode(out, &pem.Block{Type: "CERTIFICATE", Bytes: cert}); err != nil {
		return err
	}
	if err = ioutil.WriteFile(a.certFile, out.Bytes(), os.FileMode(0660)); err != nil {
		return err
	}

	return err
}

type createPrivateKeyAction struct {
	certsDir,
	keyFile string
}

// NewCreatePrivateKeyAction docs
func NewCreatePrivateKeyAction(certsDir, certName string) actions.Action {
	return &createPrivateKeyAction{
		certsDir: certsDir,
		keyFile:  path.Join(certsDir, fmt.Sprintf("%s.key", certName)),
	}
}

// Execute docs
func (a *createPrivateKeyAction) Execute(ctx actions.ActionContext) (err error) {
	var priv *rsa.PrivateKey
	ctx.Status().Start(fmt.Sprintf("Creating private key: %s", a.keyFile))
	defer func() {
		ctx.Status().End(err == nil)
		if err != nil {
			_ = os.Remove(a.keyFile)
		}
	}()
	if ctx.IsDryRun() {
		return nil
	}

	if actions.FileExists(a.keyFile) {
		return nil
	}

	if priv, err = rsa.GenerateKey(rand.Reader, 4096); err != nil {
		return err
	}

	out := &bytes.Buffer{}
	if err = pem.Encode(out, pemBlockForKey(priv)); err != nil {
		return err
	}
	err = ioutil.WriteFile(a.keyFile, out.Bytes(), os.FileMode(0600))

	return err
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to marshal ECDSA private key: %v", err)
			os.Exit(2)
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}
